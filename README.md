Deep Thought
============

Deep Thought is a Ruby on Rails application designed to solve the Ultimate Question of Life, the Universe, and Everything.

Assignment:
-----------

Development has given you this application which has become a vital tool to our
rapidly expanding customers. As a member of the Production Engineering team your
mission is to automate the application deployment using Ansible.

Details:
--------

-   You should setup a GitHub or GitLab account for your code.
    -   We expect you to commit frequently so we see all your work (including
        mistakes).
    -   We will want a link to your git repository when you turn in the homework.

Review:
-------

-   We will clone your repository to an EC2 instance to deploy the application,
    troubleshoot, and discuss.
-   Additionally you might want to be prepared to discuss the following topics
    -   Ways this application could be made reasonably resilient (i.e. a single
        node failure does not affect end users).
    -   Ways this application could be scaled, automatically if possible, to
        handle increased loads.

Bonus:
------

-   Write a script or scripts which provide basic monitoring for the services
    that run this application and deploy them along with the application via
    Ansible


[Additional
Research](http://en.wikipedia.org/wiki/Phrases\_from\_The\_Hitchhiker%27s\_Guide\_to\_the\_Galaxy\#Answer\_to\_the\_Ultimate\_Question\_of\_Life.2C\_the\_Universe.2C\_and\_Everything\_.2842.29)
