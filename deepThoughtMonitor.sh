#!/bin/bash

checkCron=`crontab -l | grep deepThoughtMonitor | grep -v grep`
deepPid=`ps -ef | grep rails | grep -v grep`
deepLogs=`ls -d ~/deep-thought/deepThoughtMonitor_Logs`

function check_cron_status() {
  if [[ -z $checkCron ]]
  then
    echo "Currently Deep Thought Monitor is not in a cron and can only be run on demand"
    echo "Should you like to rectify this, please do the following:"
    echo "1. > crontab -e"
    echo "2. Paste the following"
    echo "*/5 * * * * ~/deep-thought/deepThoughtMonitor.sh >> ~/deep-thought/deepThoughtMonitor_Logs/deepThoughtMonitor.log.`date +%\F` 2>&1"
    echo ""
    echo "3. Hit Esc"
    echo "4. Type ':wq'"
    echo "5. Hit Enter"
    echo ""
  fi
}

function check_deep_thought() {
   if [[ -z $deepPid ]]
   then
     echo "[ERROR] [`date +%F" "%H:%M:%S`] The Golgafrinchans has struck again!!!!"
     echo "This time they brought down Deep Thought"
     echo ""
     echo "Please contact your PhishMe Administrator at your earliest convenience"
     echo ""
     echo "Thank you and sorry for the inconvenience"
     echo ""
     exit
   else
     echo "INFO [`date +%F" "%H:%M:%S`] Status Code(42) - All is Well, and Deep Thought is working..."
   fi
}

if [ ! -d $deepLogs ]
then
  mkdir ~/deep-thought/deepThoughtMonitor_Logs
fi

check_cron_status
check_deep_thought
